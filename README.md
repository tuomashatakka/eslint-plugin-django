
# Django ESLint plugin

<em>
  Inline script should be avoided at any means imaginable.
  When it can't be avoided it should at very least be linted.
</em>

<em>
  ESLint can't handle HTML – let alone template tags or
  variables found in Django applications.
</em>

## How to get

Install the package with
`npm i https://gitlab.com/tuomashatakka/eslint-plugin-django --save`. Then include "django" under your eslint configuration's plugins section, e.g.

```
"eslintConfig": {
  …,
  plugins: [ "django", /* … other plugins */ ],
  …
}
```

Then just run eslint normally; `eslint ./public/index.html` or whatever.

## What it does

This plugin makes any inlined ES accessible for the ESLint linter by

1. Finding all `<script>` tags in the file
2. Stripping any template tags or variables in the code
3. Providing the resulting code to the linter

The plugin does nothing but above. No magic, no surprise but only sex. No animals were harmed in the making.

## Side note

Nothing prevents you from using this plugin with any html files with javascript inlined – the package works on plain html without any django tags just as well as it does on files with django templates included.
