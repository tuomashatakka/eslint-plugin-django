
module.exports = {
  PREFIXED_SCRIPT: /((?:.|\n)*?)<(?:\s|\n)*?script(?:[^>]|\n)*?>(((?!<\/(?:\s|\n)*?script(?:\s|\n)*?>).|\n)+)/mg,
  SCRIPT: /<(?:\s|\n)*?script(?:[^>]|\n)*?>(((?!<\/(?:\s|\n)*?script(?:\s|\n)*?>).|\n)+)/mg,
  TAG:    /({%\s*comment\s*%}(.|\n)*?{%\s*endcomment\s*%}|{({|%|#)\s*(?:.+?)\s*(}|%|#)})/mg,
}
