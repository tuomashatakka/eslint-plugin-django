
const parser    = require('./parser');
const MATCH     = require('./match');
const EXTENSIONS = [
  '.html',
  '.js',
]

// Helper functionlets
const exists    = message => message ? true : false
const lines     = message => (message.match(/\n/g) || []).length
const stripTags = src => src.replace(MATCH.TAG, '')
const reducer   = (arr, message) => arr.concat(message.map(parser).filter(exists))
const applyProc = (obj, key) => Object.assign(obj, { [key]: proc })
const linebreak = n => Array(n).fill('\n').join('')
const extrude   = (content, pattern, fn) => {

  const source  = []
  const extract = (...arg) => source.push(fn(...arg))

  content.replace(
    pattern,
    extract
  )
  return source
}


/*
  ❮ ☂ ❯
  Main
*/

function preprocess (content, filename) {
  let iter = 0

  // For js files, it's enough to just strip template tags
  if (filename.endsWith('.js'))
    return [ stripTags(content) ]

  // Lines between script tags are practically replaced
  // with bare linebreaks by this function
  const handler = (full, pre, content) => {
    let prefix = linebreak(iter + lines(pre))
    iter += lines(full)
    return prefix + content
  }

  // Extract the contents of script tags into an array
  // and ultimately return the resulting array with
  // any django's template tags stripped from the contents
  return extrude(content, MATCH.PREFIXED_SCRIPT, handler).map(stripTags)
}


// The `messages` argument contains two-dimensional array of Message objects
// where each top-level array item contains array of lint messages related
// to the text that was returned in array from preprocess() method
// you need to return a one-dimensional array of the messages you want to keep
function postprocess (messages, filename) {
  return messages.reduce(reducer.bind(filename), [])
}


const proc = {
  preprocess,
  postprocess
}

const processors = EXTENSIONS.reduce(applyProc, {})

module.exports = { processors }
// const extract = (full, match) => source.push(match)
// WAS: content.replace(MATCH.SCRIPT, extract)
