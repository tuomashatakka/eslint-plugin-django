/**
 * Takes a Message object and returns
 * the parsed version of that message
 * @method parse
 * @param  {Object} message linter's resulting object
 * @return {String} Cleaned text
 */

module.exports = function parser ({ message, fatal, severity, source, line, column }) {
  return { message, fatal, severity, source, line, column }
}
