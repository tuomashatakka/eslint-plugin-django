
## 0.2.0 (2017-08-13)

- Include js files in processors
- Code cleanup big time
- Line numbers counting corrected so that they are valid in all situations
