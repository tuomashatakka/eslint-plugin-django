function assertBound (instance) {
  if (instance instanceof require('./model'))
    return true
  throw new Error('assertion not bound')
  return false
}

// Assert that n (or > 0 if !n) errors were found while linting
function assertErrors (n=null) {
  assertBound(this)
  let e = this.errors
  return n === null ? e > 0 : e == n
}

// Assert that n (or > 0 if !n) warnings were found while linting
function assertWarnings (n=null) {
  assertBound(this)
  let e = this.warnings
  return n === null ? e > 0 : e == n
}

// Assert that a total of n (or > 0 if !n) warnings and errors
// were found while linting
function assertExceptions (n=null) {
  assertBound(this)
  let e = this.exceptions
  return n === null ? e > 0 : e == n
}


module.exports = {
  assertErrors,
  assertWarnings,
  assertExceptions,
}
