const delim = '\n'
class LintResults {

  constructor ({ results, errorCount, warningCount }, expr=null) {
    this.expression = expr
    this.warnings   = warningCount
    this.errors     = errorCount
    this.results    = results
    let assertions  = require('./assertions')

    for (let name in assertions) {
      let fn = assertions[name]
      this[name] = fn.bind(this)
    }
  }

  hasFatalErrors () {
    return this.messages.find(item => item.fatal) ? true : false
  }

  get exceptions () {
    return this.errors + this.warnings
  }

  get message () {
    return this.results.map(result => result.message).join(delim)
  }

}

module.exports = LintResults
