
const { CLIEngine: Runner } = require('eslint')
const { readdirSync: ls }   = require('fs')
const { resolve: path }     = require('path')
const chalk                 = require('chalk')
const LintResults           = require('./model')

const conf = {
  useEslintrc: false,
  envs:    [ 'browser' ]}

let
  SUCCESS,
  FAILED,
  SUCCESS_TEXT = '◯ pass',
  ERROR_TEXT = '⊗ FAILED'

try {
  SUCCESS = chalk.hex('#31e4ae').bold(SUCCESS_TEXT)
  FAILED  = chalk.hex('#f14060').bold(ERROR_TEXT) }

catch (e) {
  console.warn(e)
  SUCCESS = SUCCESS_TEXT
  FAILED = ERROR_TEXT }

const totals       = []
const files        = [ 'index.html' ]
const runner       = new Runner(conf)
const SUCCESS_MSG  = 'Passed'
const absolutePath = fp => path(__dirname, fp)
const plugin = require('../src/index')
console.log(plugin)

function getFileResults (file) {
  let res = runner.executeOnFiles([ absolutePath(file) ])
  return new LintResults(res, file)
}

function evaluate (...input) {
  let results = runner.executeOnText(input.join('\n'), 'test.html')
  return new LintResults(results, input)
}


function assert (res, ...args) {

  if (!res.expression)
    res = evaluate(res)

  const test      = res.expression
  const run       = (desc, fn, ...args) => {

    let result

    const fail    = message => ({ test: res.expression + ' ' + desc, message, result: FAILED })
    const success = message => ({ test: res.expression + ' ' + desc, message: res.message || SUCCESS_MSG, result: SUCCESS })

    try {
      result = fn(...args) ?
        success(result !== true ? result : null) :
        fail('Assertion failed')
    }

    catch (e) { result = fail(e.toString()) }
    totals.push(result)
  }

  return {
    errors:     n => run(n === 0 ? "does not throw errors" : "throws an error", res.assertErrors, n),
    warnings:   n => run(n === 0 ? "does not raise warnings" : "throws a warning", res.assertWarnings, n),
    exceptions: n => run(n === 0 ? "passes" : "does not commute", res.assertExceptions, n),
  }
}


function parseResult ({ test, message, result }, n) {
  const name   = test.toString()
  const out    = (...msg) => console.log(msg.join(""))
  const indent = (s, max=32) => {
    let len    = Math.max(1, max - s.length)
    return new Array(len).fill('').join(' ') }
  out(
    '∫ ', n, indent(n.toString(), 4), name, indent(name, 80), ' ', result, indent(result, 8)
  )
}

function main () {
  // let res = getResults(...files)
  // assert(res.assertErrors, 0)

  // First evaluate tests without the plugin
  assert(`var x = :0`).errors()
  assert(`<script>asdasd))(var x = 0xd!</script>`).exceptions()
  assert(`<script>var x = {{xd}}40;</script>`).exceptions()
  assert(`<script>{# comment#}var x = {{xd}}40;</script>`).exceptions()
  assert(`<script>x =d:{{xd}}40;</script>`).exceptions()
  assert(`<script>cat = {%include "yiss.html"%};</script>`).errors()

  // Then with the plugin active
  runner.addPlugin("eslint-plugin-django", plugin)

  assert(`<script>{# comment#}var x = {{xd}}40;</script>`).exceptions(0)
  assert(`<script>x=d:{{xd}}40;</script>`).exceptions()
  assert(`<script>cat = '{%include "yiss.html"%}';</script>`).errors(0)
  assert(`<script>cat = {%include "yiss.html"%};</script>`).errors()
  assert(`<script>cat = {%include "yiss.html"%}'asd';</script>`).errors(0)
  assert(`


    abc
    kissa



    lol<script>

    cat = {%include "yiss.html"%};</script>



    lol<script>

    cat = {%include "yiss.html"%};</script>
    <script>cat = {%include "yiss.html"%};</script><script>
    cat = {%include "yiss.html"%};
</script>`).errors()

}

// Evaluate
main()
totals.map(parseResult)

if (totals.filter(item => item.result === FAILED).length)
  process.exit(1)

process.exit()
